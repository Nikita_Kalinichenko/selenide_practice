package com.testing.selenideintro.csstask;

import com.codeborne.selenide.Configuration;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selenide.*;

public class TodoMvcTest {
    @Ignore
    void completesTask() {
        Configuration.browser = "firefox";
        Configuration.startMaximized = true;
        //open TodoMVC page
        open("http://todomvc.com/examples/emberjs/");

        // add tasks: "a", "b", "c"
        $("#new-todo").shouldBe(visible).setValue("a").pressEnter();
        element(byId("new-todo")).setValue("b").pressEnter();
        element("#new-todo").setValue("c").pressEnter();

        // tasks should be "a", "b", "c"
        elements("#todo-list>li").shouldHave(exactTexts("a","b","c"));

        // toggle b
        // element("li:nth-child(2) .toggle").click();
        elements("#todo-list li").findBy(exactText("b")).find(".toggle").click();

        // completed tasks should be b
        // css selector
        //elements(".completed").shouldHave(exactTexts("b"));
        $$("#todo-list li").filterBy(cssClass("completed"))
                .shouldHave(exactTexts("b"));

        /* active tasks should be a, c */
        //elements("#todo-list li:not(.completed)").shouldHave(exactTexts("a", "c"));
        $$("#todo-list li").filterBy(not(cssClass("completed")))
                .shouldHave(exactTexts("a", "c"));

    }

}
