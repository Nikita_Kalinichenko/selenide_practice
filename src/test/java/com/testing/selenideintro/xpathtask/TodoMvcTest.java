package com.testing.selenideintro.xpathtask;

import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.CollectionCondition.*;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;

public class TodoMvcTest {
    @Test
    void completesTask() {
        // open TodoMVC page
        open("http://todomvc.com/examples/emberjs/");

        // add tasks: "a", "b", "c"
        $("#new-todo").val("a").pressEnter();
        $("#new-todo").val("b").pressEnter();
        $("#new-todo").val("c").pressEnter();

        // tasks should be "a", "b", "c"
        $$("#todo-list>li").shouldHave(exactTexts("a","b","c"));

        // toggle b
        // element(byXpath("//*[@id='todo-list']//li[.//text()='b']//*[contains(concat('', normalize-space(@class), ' '), 'toggle')]")).click();
        element(byXpath("//*[@id='todo-list']//li[.//text()='b']//*[@class='toggle']")).click();

        // completed tasks should be b
        $(byXpath("//*[@id='todo-list']//li[@class='completed ember-view']//label")).shouldHave(exactText("b"));

        // active tasks should be a, c
        $$(byXpath("//*[@id='todo-list']//li[@class='ember-view']//label")).shouldHave(exactTexts("a","c"));

    }

}
